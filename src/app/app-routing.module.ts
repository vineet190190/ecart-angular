import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { BlogComponent } from './blog/blog.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { AuthGuard } from './guards/auth.guard';
import { ParentComponent } from './parent/parent.component';

const routes: Routes = [
    {path:'', component:RegisterComponent},
    {path:'course', component:CoursesComponent, canActivate: [AuthGuard]},
    {path:'course/:id', component:CourseDetailComponent, canActivate: [AuthGuard]},
    {path:'hooks', component:BlogComponent, canActivate: [AuthGuard]},
    {path:'home', component:HomeComponent, canActivate: [AuthGuard]},
    {path:'parent', component:ParentComponent, canActivate: [AuthGuard]},   
    {path:'register', component:RegisterComponent},
    
    
]

const routerOptions: ExtraOptions = {
  useHash: true,
  anchorScrolling: 'enabled',
  // ...any other options you'd like to use
};

@NgModule({ 
  imports: [
    RouterModule.forRoot(routes, routerOptions),   
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
    CoursesComponent,
    BlogComponent,
    HomeComponent, 
    RegisterComponent, 
    CourseDetailComponent
]