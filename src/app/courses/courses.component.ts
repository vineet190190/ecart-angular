import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router} from '@angular/router';
import { AlertService } from '../services/alert.service';
import { reject } from 'q';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courseModel = {
    "title":"Laravel" ,
    "userId":1    
  }; 
  

  public courses;
  constructor(
    private userservice: UserService,
    private router: Router,
    private alertService: AlertService
    ) { 
    
    // this.userservice.getCourse()
    //  .subscribe((res : any[])=>{
    //     this.courses = res;        
    //     console.log(this.courses)
    // });

    this.userservice.getCourse()
    .then(
      res => {
        this.courses = res;console.log(res)
        //this.courses = res.json().courses;
        //resolve();
      },
      msg=>{
        console.log(msg.message)
      }
    );
    
  }

  ngOnInit() {
  }

  onSelect(course){
    
    
    this.router.navigate(['/course', course.id])    
  }

  onSubmit(){
    console.log(this.courseModel.title)
    if (this.courseModel.title){
      this.userservice.submit(this.courseModel, 'course')
      .subscribe(
      data => {
        this.alertService.error('Success fully added');
      },
      error => console.error('Error!', error)
    )
    }
    
  }


}
