import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  
  public nav = [];
  constructor(private userservice: UserService ) { 

    
    this.nav  = this.userservice.getMenu();
    
   
  }

  ngOnInit() {
  }

  


}
