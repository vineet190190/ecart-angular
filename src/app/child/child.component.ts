import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }
  @Input() childMessage = 'child message'

  @Output() sendToParent = new EventEmitter<string>();
 
  ngOnInit() {
    console.log('ngOnInit is working child')
  }

  ngOnChanges() {
    console.log('ngOnChanges is working chiild')

    this.sendToParent.emit('send to parent')
    
  }
}
