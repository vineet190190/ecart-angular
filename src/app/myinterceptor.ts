import { Injectable } from '@angular/core';
import { ErrorDialogService } from './services/errordialog.service';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';

@Injectable()
export class Myinterceptor implements HttpInterceptor {
    constructor(public errorDialogService: ErrorDialogService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        localStorage.setItem('token',"test");
        const token: string = localStorage.getItem('token');
        
        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        if (!request.headers.has('username')) {
            request = request.clone({ headers: request.headers.set('username', 'admin') });
        }

        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

        
        return next.handle(request).pipe(
            //retry(2),
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('catch in interceptor');
                    //this.errorDialogService.openDialog(event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                console.log('catch error in interceptor');
                let data = {};
                data = {
                    reason: error && error.error.message ? error.error.message : 'Messgae not defined',
                    status: error.status
                };
                this.errorDialogService.openDialog(data);
                return throwError(error);
            }));
    }
}