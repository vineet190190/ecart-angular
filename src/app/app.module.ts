import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';


import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AlertComponent } from './alert/alert.component';
import { Myinterceptor } from './myinterceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material';
import { ErrordialogComponent } from './errordialog/errordialog.component';

import { UserService } from './services/user.service';
import { ErrorDialogService } from './services/errordialog.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';

@NgModule({
  declarations: [
    AppComponent,    
    HeaderComponent,
    FooterComponent,
    routingComponents,
    AlertComponent,
    ErrordialogComponent,
    ParentComponent,
    ChildComponent       
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule    
  ],
  providers: [
    AuthGuard,    
    UserService,
    AuthService,
    ErrorDialogService,
    {provide:HTTP_INTERCEPTORS, useClass:Myinterceptor, multi:true}
  ],
  entryComponents: [ErrordialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
