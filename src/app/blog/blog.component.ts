import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent 
implements 
OnInit, 
OnChanges, 
DoCheck, 
AfterContentInit, 
AfterContentChecked, 
AfterViewInit,
AfterViewChecked, 
OnDestroy {

  hooks : string[] = [];
  joke = 'Hook testing';
  
  constructor() {
   this.hooks.push("Constuctor Invoked ")
    console.log('blog component created')
  }

  ngOnChanges() {
    this.hooks.push("ngOnChanges Invoked ")
    console.log('blog component ngOnChanges')
}

  ngOnInit() {
    this.hooks.push("ngOnInit Invoked ")
    console.log('blog component ngOnInit')
  }

  ngDoCheck() {
    this.hooks.push("ngDoCheck Invoked ")
    console.log("ngDoCheck");
  }

  ngAfterContentInit() {
    this.hooks.push("ngAfterContentInit Invoked ")
    console.log("ngAfterContentInit");
  }

  ngAfterContentChecked() {
    this.hooks.push("ngAfterContentChecked Invoked ")
    console.log("ngAfterContentChecked");
  }

  ngAfterViewInit() {
    this.hooks.push("ngAfterViewInit Invoked ")
    console.log("ngAfterViewInit");
  }

  ngAfterViewChecked() {
    this.hooks.push("ngAfterViewChecked Invoked ")
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy() {
    this.hooks.push("ngOnDestroy Invoked ")
    console.log("ngOnDestroy");
}


}
