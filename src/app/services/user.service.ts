import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  //baseUrl:string = "http://localhost:8080/api/";
  baseUrl:string = "https://ecart-backend.herokuapp.com/api/";
  
  
  constructor(
    private _http: HttpClient
  ) { }

  

  getUser() {
    return this._http.get(this.baseUrl+'user');    
  }

  getMenu() {
    return [      
      {"id" : "1", "title" : "Home", "path": "home" },  
      {"id" : "2", "title" : "Signup", "path": "register" },       
      {"id" : "3", "title" : "Course", "path": "course" }, 
      {"id" : "4", "title" : "Hooks", "path": "hooks" }
  ];    
  }

  getCourse() {
    return this._http.get(this.baseUrl+'course').toPromise(); 
       
  }

  getCourseDetail(courseId,filter=''){   
    if (filter && filter != '*'){
      return this._http.get(this.baseUrl+'topic/'+courseId+"?filter="+filter); 
    }
    return this._http.get(this.baseUrl+'topic/'+courseId); 
  }

  getTopicDetail(courseId, topicId){   
    return this._http.get(this.baseUrl+'topic/'+courseId+'?id='+topicId); 
  }

  deleteTopic(FormData){   
    console.log(FormData)
    return this._http.put(this.baseUrl+'topic/delete', FormData);
     
  }

  

  submit(FormData, url){
   
    if (FormData.updateId){
      return this._http.put(this.baseUrl+url, FormData);
    }else{
      return this._http.post(this.baseUrl+url, FormData);
    }
   

    //return this._http.post(this.baseUrl+url, FormData, this.httpOptions)
    //.subscribe((res) => {
     // console.log(res)
     //});
    
    
  }


  


}
