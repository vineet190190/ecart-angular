import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { FormBuilder, FormGroup, FormArray} from '@angular/forms';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
  title = 'Angular Tutorial'
  courseDetail;
  topicDetail;
  topicForm: FormGroup;
  topicLists = [];
  courseId;
  isSubmit;
  isForm;  

 

 get metaForms() {
  return this.topicForm.get('meta') as FormArray
 }

 addMeta() {

  const meta = this.fb.group({ 
    title: [],
    desc: []    
  })

  this.metaForms.push(meta);
}

deleteMeta(i) {
  this.metaForms.removeAt(i)
}

  constructor(
    private userservice: UserService, 
    private route: ActivatedRoute,
    private fb: FormBuilder
    ) { }

  ngOnInit() {
     
    this.topicForm = this.fb.group({
      title: [''],
      about: [''],     
      courseId: parseInt(this.route.snapshot.paramMap.get('id')),
      userId: 1,     
      meta: this.fb.array([])
    })
      

    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'));
    
    this.userservice.getCourseDetail(this.courseId)
    .subscribe(
      (res : any[])=>{
                  
                    res.forEach(element => {    
                    let dataIsthere = false;
                      if (element.about){
                        dataIsthere = true
                    }    
                    this.topicLists.push({'title' : element['title'], 'id' : element['id'], 'dataIsthere' : dataIsthere, 'imp':element['imp']});
                    element['meta'] = JSON.parse(element['meta']); 
                  }),
      (err : any[]) => {
        console.log(err)
      }            
      
  });
  //this.courseDetail = null
  }

  onChange(event,filter){
    this.topicLists = []
    
    this.userservice.getCourseDetail(event.courseId,filter)
    .subscribe(
      (res : any[])=>{
                  
                    res.forEach(element => {    
                    let dataIsthere = false;
                      if (element.about){
                        dataIsthere = true
                    }    
                    this.topicLists.push({'title' : element['title'], 'id' : element['id'], 'dataIsthere' : dataIsthere, 'imp':element['imp']});
                    element['meta'] = JSON.parse(element['meta']); 
                  }),
      (err : any[]) => {
        console.log(err)
      }      
  })
  }

  onSelect(topic){
    console.log('course detail')
    this.isForm = false
    
    this.userservice.getTopicDetail(this.courseId, topic.id).subscribe((res : any[])=>{
      this.topicDetail = res; 
      this.topicDetail.forEach(element => {       

        
        element['meta'] = JSON.parse(element['meta']);  
        
      });      
      this.courseDetail = this.topicDetail
     
  });
  }

  
  edit(topic){
    
    this.isForm = true;
    this.topicForm = this.fb.group({
      title: [topic.title],
      about: [topic.about],      
      courseId: [topic.courseId],
      userId: [topic.userId],
      updateId: [topic.id],     
      meta: this.fb.array([])
    })
    
    topic.meta.forEach(element => {      
        
      let meta = this.fb.group({ 
        title: [element.title],
        desc: [element.desc]    
      })
      this.metaForms.push(meta);
      
    });
    
  }

  resetForm(){
    console.log('reset')
    this.isForm = true
    this.topicForm = this.fb.group({
      title: [''],
      about: [''],     
      courseId: parseInt(this.route.snapshot.paramMap.get('id')),
      userId: 1,     
      meta: this.fb.array([])
    })
  }

  onSubmit(){
    this.isForm = false
    this.userservice.submit(this.topicForm.value, 'topic')
    .subscribe(
      data => {
        this.isSubmit = 'Successfully submit'    
        setTimeout(()=>{   
          this.isSubmit = false;
     }, 3000);    
      },
      error => console.error('Error!', error)
    )
  }
  
  imp(topic){
    let FormData: object = {
      imp:topic.imp + 1,
      updateId: topic.id
    };
    // console.log(FormData)
    this.userservice.deleteTopic(FormData)
    .subscribe(
      data => {
        this.isSubmit = 'Marked important ' + (topic.imp + 1)
        setTimeout(()=>{   
          this.isSubmit = false;
     }, 3000);    
      },
      error => console.error('Error!', error)
    )
  }

  delete(topic){
    
    if(confirm("Are you sure to delete "+topic.title)) {
      console.log("Implement delete functionality here");
      let FormData: object = {
        status:9,
        updateId: topic.id
      };
      
      
     // console.log(FormData)
      this.userservice.deleteTopic(FormData)
    .subscribe(
      data => {
        this.isSubmit = 'Successfully delete'    
        setTimeout(()=>{   
          this.isSubmit = false;
     }, 3000);    
      },
      error => console.error('Error!', error)
    )

    }
  }

}
