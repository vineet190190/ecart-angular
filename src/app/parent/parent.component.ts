  import { Component, OnInit } from '@angular/core';

  @Component({
    selector: 'app-parent',
    templateUrl: './parent.component.html',
    styleUrls: ['./parent.component.css']
  })
  export class ParentComponent implements OnInit {

    constructor() { }
    parentMessage = 'parent message'
    recievedMessage = ''
    ngOnInit() {
      console.log('ngOnInit is working on parent')
    }

    ngOnChanges(){
      console.log('ngOnChanges is working parent')
    }

    parentRecieved($event){
      console.log('parentRecieved')
      this.recievedMessage = $event
      
    }


  }
